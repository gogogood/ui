import React from 'react'

import { withRouter } from 'react-router-dom'

import style from './RestaurantCard.css'

const restaurantCard = props => {
  return (
    <div onClick={props.handleClicked}>
      <div className={style.Card}>
        <img src={props.data.photoPola} alt='' />
        <h3>{props.data.name}</h3>
        <h4>{props.data.street} {props.data.district} {props.data.city}</h4>
        <p>{props.data.catchPhrase}</p>
      </div>
    </div>
  )
}

export default withRouter(restaurantCard)
