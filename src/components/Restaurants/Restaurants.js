import React, { useEffect, useCallback } from 'react'
import MetaTags from 'react-meta-tags'

import { useDispatch, useSelector } from 'react-redux'
import * as allActions from '../../store/actions'

import api from '../../axios-api'

import { withRouter } from 'react-router-dom'

import RestaurantCard from './RestaurantCard/RestaurantCard'
import Loader from '../../components/UI/Loader/Loader'

import style from './Restaurants.css'

const Restaurants = props => {
  const {
    fetchRestaurantsStart,
    fetchRestaurantsSuccess,
    fetchRestaurantsFail
  } = props

  const fetchedRestaurants = useSelector(state => state.fetchRestaurants.restaurants)
  const displayValue = useSelector(state => state.fetchRestaurants.selectedCategory.displayValue)
  const extension = useSelector(state => state.fetchRestaurants.selectedCategory.extension)

  const dispatch = useDispatch()
  const onUpdateSelectedRestaurant = useCallback((restaurant) => dispatch(allActions.updateSelectedRestaurant(restaurant)))
  const onfetchRestaurantsStart = () => dispatch(allActions.fetchRestaurantsStart())
  const onfetchRestaurantsSuccess = useCallback((restaurants) => dispatch(allActions.fetchRestaurantsSuccess(restaurants)), [dispatch])
  const onfetchRestaurantsFail = () => dispatch(allActions.fetchRestaurantsFail())

  const query = props.history.location.pathname.slice(13)

  console.log('Reloading All restaurants component')
  console.log(query)

  const statusCode = null

  const fetchRestaurants = () => {
    onfetchRestaurantsStart()
    api.get(`/posts/district/${query}`)
      .then(response => {
        onfetchRestaurantsSuccess(response.data.posts)
      })
      .catch((error) => {
        console.log(error)
        onfetchRestaurantsFail()
        if (error.response.status === 404) {
          props.history.replace('/404')
        }
      })
  }

  useEffect(() => {
    if (fetchedRestaurants.length === 0) {
      fetchRestaurants()
    } else {
      if (query !== fetchedRestaurants[0].category) {
        fetchRestaurants()
      }
    }
  }, [
    fetchRestaurantsStart,
    fetchRestaurantsSuccess,
    fetchRestaurantsFail,
    query
  ])

  const onGetRestaurantHandler = (restaurant) => {
    onUpdateSelectedRestaurant(restaurant)
    const path = props.history.location.pathname[props.history.location.pathname.length - 1]
    const slash = path === '/' ? '' : '/'
    props.history.replace(props.history.location.pathname + slash + restaurant._id)
  }

  let restaurants = <div className={style.LoaderContainer}><Loader width='400' /></div>

  if (fetchedRestaurants) {
    restaurants = fetchedRestaurants
      .map(restaurant => {
        if (restaurant.category === query) {
          return <RestaurantCard key={restaurant._id} data={restaurant} id={restaurant._id} handleClicked={() => onGetRestaurantHandler(restaurant)} />
        }
      })
      .sort((a, b) => {
        return new Date(b.props.data.creationDate) - new Date(a.props.data.creationDate)
      })
  }

  if (fetchedRestaurants && statusCode === 404) {
    restaurants = <p>No restaurants here !</p>
  }

  return (
    <>
      <MetaTags>
        <title>Gogogood | {displayValue}</title>
        <meta name='description' content={'Tous les restaurants sélectionnés par Gogogood à ' + displayValue} />
        <meta property='og:title' content={'Gogogood | ' + displayValue} />
      </MetaTags>
      <main className={style.MainRestaurants}>
        <h2>{displayValue}<sup>{extension}</sup></h2>
        <div className={style.Cards}>
          {restaurants}
        </div>
      </main>
    </>
  )
}

export default withRouter(Restaurants)
