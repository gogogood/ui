import React, { useEffect, useCallback } from 'react'

import { useDispatch, useSelector } from 'react-redux'
import * as allActions from '../../store/actions'

import api from '../../axios-api'

import { withRouter } from 'react-router-dom'
import Input from '../../components/UI/Input/Input'
import Loader from '../../components/UI/Loader/Loader'

const SelectInput = props => {
  const dispatch = useDispatch()

  const selectedCategory = useSelector(state => state.fetchRestaurants.selectedCategory)
  const categoriesFetched = useSelector(state => state.fetchRestaurants.categories)
  const selectInputData = useSelector(state => state.fetchRestaurants.selectInputData)

  const onUpdateSelectedInput = useCallback((selectedCategory) => dispatch(allActions.updateSelectedCategory(selectedCategory)))
  const onfetchCategoriesStart = () => dispatch(allActions.fetchCategoriesStart())
  const onfetchCategoriesSuccess = useCallback((opts) => dispatch(allActions.fetchCategoriesSuccess(opts)), [dispatch])
  const onfetchCategoriesFail = () => dispatch(allActions.fetchCategoriesFail())
  const onfetchCategoriesSelectInputStart = () => dispatch(allActions.fetchCategoriesSelectInputStart())
  const onfetchCategoriesSelectInputSuccess = useCallback((opts) => dispatch(allActions.fetchCategoriesSelectInputSuccess(opts)), [dispatch])
  const onfetchCategoriesSelectInputFail = () => dispatch(allActions.fetchCategoriesSelectInputFail())

  const paramEdited = props.location.pathname.split('/')[2]

  useEffect(() => {
    if (categoriesFetched === null) {
      onfetchCategoriesStart()
      onfetchCategoriesSelectInputStart()
      api.get('/categories/all')
        .then(response => {
          onfetchCategoriesSuccess(response.data.categories)
          onfetchCategoriesSelectInputSuccess(response.data.categories)
          if (selectedCategory.category === null) {
            const displayValueTransformed = response.data.categories.filter(category => {
              return category.slug === paramEdited
            })
            const updateSelectedCategory = {
              category: displayValueTransformed[0].slug,
              displayValue: displayValueTransformed[0].displayValue,
              extension: displayValueTransformed[0].extension
            }
            onUpdateSelectedInput(updateSelectedCategory)
          }
        })
        .catch(error => {
          onfetchCategoriesFail()
          onfetchCategoriesSelectInputFail()
          console.log(error)
        })
    }
  }, [])

  const inputChangedHandler = (event) => {
    const displayValueTransformed = categoriesFetched.find(category => {
      return category.slug === event.target.value
    })
    const updateSelectedCategory = {
      category: displayValueTransformed.slug,
      displayValue: displayValueTransformed.displayValue,
      extension: displayValueTransformed.extension
    }
    onUpdateSelectedInput(updateSelectedCategory)
    props.history.replace(`/restaurants/${event.target.value}`)
  }

  const formElementArray = []
  for (const key in selectInputData) {
    formElementArray.push({
      id: key,
      config: selectInputData[key]
    })
  }

  let input = formElementArray.map((formElement) => (
    <Input
      type={props.type}
      key={formElement.id}
      elementType={formElement.config.elementType}
      elementConfig={formElement.config.elementConfig}
      value={formElement.config.value}
      invalid={!formElement.config.valid}
      shouldValidate={formElement.config.validation}
      touched={formElement.config.touched}
      handleChanged={(event) => inputChangedHandler(event, formElement.id)}
    />
  ))

  if (!categoriesFetched) {
    input = <Loader width='100' />
  }

  return input
}

export default withRouter(SelectInput)
