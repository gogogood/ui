import React from 'react'

import { Link, withRouter } from 'react-router-dom'

import Logo from '../UI/Logo/Logo'
import SelectInput from '../SelectInput/SelectInput'

import style from './ErrorPage.css'

import {
  Wrapper,
  LeftCutImg,
  LeftVerticalImg,
  MddlBox,
  MddlTopHoriImg,
  MddlBottomBox,
  MddlBttmLeftWrap,
  MddlBttmLeftBottomImg,
  MddlBttmLeftVerticalImg,
  RightCutImg
} from './ErrorPageStyle'

const errorPage = props => {
  return (
    <Wrapper>
      <LeftCutImg />
      <LeftVerticalImg />
      <MddlBox>
        <MddlTopHoriImg />
        <MddlBottomBox>
          <MddlBttmLeftWrap>
            <div className={style.CtaWrapperError}>
              <div className={style.SvgBox}>
                <Link to='/'>
                  <Logo size='100' />
                </Link>
              </div>
              <p>Rien à se mettre sous la dent ici...</p>
              <div>
                <SelectInput type='WhiteBlueHov' />
              </div>
            </div>
            <MddlBttmLeftBottomImg />
          </MddlBttmLeftWrap>
          <MddlBttmLeftVerticalImg />
        </MddlBottomBox>
      </MddlBox>
      <RightCutImg />
    </Wrapper>
  )
}

export default withRouter(errorPage)
