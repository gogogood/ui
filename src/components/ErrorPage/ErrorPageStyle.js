import styled from 'styled-components'
import { device } from '../../device'

const imageLeftCutImg = 'https://res.cloudinary.com/dyywnpzbg/image/upload/v1566652499/gogogood/empty1.png'
const imageLeftVerticalImg = 'https://res.cloudinary.com/dyywnpzbg/image/upload/v1566652424/gogogood/empty2.png'
const imageMddlTopHoriImg = 'https://res.cloudinary.com/dyywnpzbg/image/upload/v1566652415/gogogood/empty3.png'
const imageMddlBttmLeftBottomImg = 'https://res.cloudinary.com/dyywnpzbg/image/upload/v1566652479/gogogood/empty4.png'
const imageMddlBttmLeftVerticalImg = 'https://res.cloudinary.com/dyywnpzbg/image/upload/v1566652445/gogogood/empty5.png'
const imageRightCutImg = 'https://res.cloudinary.com/dyywnpzbg/image/upload/v1566652466/gogogood/empty6.png'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column-reverse;
  background-color: #222E50;
  
  @media ${device.laptop} {
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: space-between; 
    flex-direction: row;
  }
`

export const LeftCutImg = styled.div`
  @media ${device.laptop} {
    border-top: 16px solid #222E50;
    border-right: 8px solid #222E50;
    border-bottom: 16px solid #222E50;
    border-left: 0;
    flex: 0 1 10%;
    background-image: url("${imageLeftCutImg}");
    padding: 16px;
    background-position: center;
  }
`

export const LeftVerticalImg = styled.div`

  @media ${device.laptop} {
    border-top: 16px solid #222E50;
    border-right: 8px solid #222E50;
    border-bottom: 16px solid #222E50;
    border-left: 8px solid #222E50;
    flex: 0 0 23%;
    background-position: center;
    background-image: url("${imageLeftVerticalImg}");
  }
`

export const MddlBox = styled.div`

  @media ${device.laptop} {
    border-top: 16px solid #222E50;
    border-right: 8px solid #222E50;
    border-bottom: 16px solid #222E50;
    border-left: 8px solid #222E50;
    flex: 0 0 57%;
    display: flex;
    flex-direction: column; 
  }
`

export const MddlTopHoriImg = styled.div`
  background-image: url("${imageMddlTopHoriImg}");
  background-size: cover;
  background-position: center;
  width: 100%;
  height: 20vh;
  @media ${device.laptop} {
    flex: 0 0 24%;
    width: auto;
    height: auto;
  }
`

export const MddlBottomBox = styled.div`

  @media ${device.laptop} {
    flex: 0 0 76%;
    display: flex;
    justify-content: space-between;
  }
`

export const MddlBttmLeftWrap = styled.div`
  height: 50vh;
  background-position: center;
  background-size: cover;
  @media ${device.laptop} {
    border-top: 16px solid #222E50;
    border-right: 8px solid #222E50;
    flex: 0 1 59%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    background-color: #222E50;
    height: auto;
  }
`

export const MddlBttmLeftBottomImg = styled.div`

  @media ${device.laptop} {
    flex: 0 0 32%;
    background-size: cover;
    background-position: center;
    background-image: url("${imageMddlBttmLeftBottomImg}");
  }
`

export const MddlBttmLeftVerticalImg = styled.div`
height: 30vh;
background-size: cover;
background-position: center;
background-image: url("${imageMddlBttmLeftVerticalImg}");
  @media ${device.laptop} {
    border-top: 16px solid #222E50;
    border-left: 8px solid #222E50;
    flex: 0 1 41%;
    height: auto;
  }
`

export const RightCutImg = styled.div`

  @media ${device.laptop} {
    border-top: 16px solid #222E50;
    border-right: 0;
    border-bottom: 16px solid #222E50;
    border-left: 8px solid #222E50;
    flex: 0 1 10%;
    background-image: url("${imageRightCutImg}");
  }
`
