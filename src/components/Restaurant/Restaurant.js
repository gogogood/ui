import React, { useEffect, useCallback } from 'react'
import MetaTags from 'react-meta-tags'

import { useDispatch, useSelector } from 'react-redux'
import * as allActions from '../../store/actions'

import api from '../../axios-api'

import { withRouter } from 'react-router-dom'

import Loader from '../../components/UI/Loader/Loader'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCompass, faMobileAlt, faClock } from '@fortawesome/free-solid-svg-icons'

import style from './Restaurant.css'

const emoji = require('emoji-dictionary')

const Restaurant = props => {
  const selectedRestaurant = useSelector(state => state.fetchRestaurants.selectedRestaurant)
  const restaurants = useSelector(state => state.fetchRestaurants.restaurants)

  const dispatch = useDispatch()
  const onUpdateSelectedRestaurant = useCallback((restaurant) => dispatch(allActions.updateSelectedRestaurant(restaurant)))

  const paramEdited = props.location.pathname.split('/')[3]

  useEffect(() => {
    if (selectedRestaurant.length !== 0 && restaurants.length !== 0) {
      console.log(selectedRestaurant)
      console.log(restaurants)
      restaurants.map(restaurant => {
        if (restaurant._id === paramEdited) {
          onUpdateSelectedRestaurant(restaurant)
        }
      })
    } else {
      api.get(`/posts/${paramEdited}`)
        .then(response => {
          console.log(response)
          onUpdateSelectedRestaurant(response.data.post)
        })
        .catch(err => {
          console.log(err)
        })
    }
  }, [])

  let restaurant = <div id={style.LoaderContainer}><Loader width='400' /></div>

  if (selectedRestaurant !== null) {
    const stars = <span>{emoji.getUnicode('cookie').repeat(selectedRestaurant.rate)}</span>
    restaurant = (
      <main className={style.MainRestaurant}>
        <div className={style.Wrapper}>
          <div className={style.Arborescence} onClick={() => props.history.push(`/restaurants/${selectedRestaurant.category}`)}>Retour à <span>{selectedRestaurant.category}</span></div>
          <div>{stars}</div>
          <h2>{selectedRestaurant.name}</h2>
          <div className={style.Informations}>
            <span><a href={selectedRestaurant.googleMapUrl} target='_blank' rel='noopener noreferrer'><FontAwesomeIcon icon={faCompass} />&nbsp;&nbsp;{selectedRestaurant.street}, {selectedRestaurant.district} {selectedRestaurant.city}</a></span>
            <span><FontAwesomeIcon icon={faMobileAlt} />&nbsp;&nbsp;{selectedRestaurant.phone}</span>
            <span><FontAwesomeIcon icon={faClock} />&nbsp;&nbsp;{selectedRestaurant.openningHours}</span>
          </div>
          <div className={style.Description1}>
            <div className={style.Text}>
              <span>{selectedRestaurant.catchPhrase}</span>
              <p>{selectedRestaurant.description}</p>
            </div>
            <div className={style.ContainerImage}>
              <img src={selectedRestaurant.Photo1} />
            </div>
          </div>
          <div className={style.Description2}>
            <div className={style.Description2Box}>
              <div className={style.Menu} style={(!selectedRestaurant.Photo2 && !selectedRestaurant.Photo3) ? { marginRight: '0px' } : {}}>
                <h3>What to eat</h3>
                <div className={style.Plate}>
                  <div className={style.Main}>
                    <span>{selectedRestaurant.plateName1}</span>
                    <span>{selectedRestaurant.platePrice1}</span>
                  </div>
                  <div className={style.Description}>{selectedRestaurant.plateDescription1}</div>
                </div>
                <div className={style.Plate}>
                  <div className={style.Main}>
                    <span>{selectedRestaurant.plateName2}</span>
                    <span>{selectedRestaurant.platePrice2}</span>
                  </div>
                  <div className={style.Description}>{selectedRestaurant.plateDescription2}</div>
                </div>
                <div className={style.Plate}>
                  <div className={style.Main}>
                    <span>{selectedRestaurant.plateName3}</span>
                    <span>{selectedRestaurant.platePrice3}</span>
                  </div>
                  <div className={style.Description}>{selectedRestaurant.plateDescription3}</div>
                </div>
              </div>
              {selectedRestaurant.Photo2 ? <div className={style.ContainerImage}><img src={selectedRestaurant.Photo2} /></div> : null}
            </div>
            {selectedRestaurant.Photo3 ? <div className={style.ContainerImage}><img src={selectedRestaurant.Photo3} /></div> : null}
          </div>
        </div>
      </main>
    )
  }

  return (
    <>
      <MetaTags>
        <title>Gogogood | {selectedRestaurant.name}</title>
        <meta name='description' content={selectedRestaurant.catchPhrase} />
        <meta property='og:title' content={'Gogogood | ' + selectedRestaurant.name} />
        <meta property='og:image' content={selectedRestaurant.Photo1} />
      </MetaTags>
      {restaurant}
    </>
  )
}

export default withRouter(Restaurant)
