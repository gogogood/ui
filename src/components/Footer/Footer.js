import React, { useCallback } from 'react'

import { useSelector, useDispatch } from 'react-redux'
import * as allActions from '../../store/actions'

import { Link } from 'react-router-dom'

import style from './Footer.css'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram } from '@fortawesome/free-brands-svg-icons'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'

const Footer = props => {
  const categoriesFetched = useSelector(state => state.fetchRestaurants.categories)

  const dispatch = useDispatch()
  const onUpdateSelectedInput = useCallback((selectedCategory) => dispatch(allActions.updateSelectedCategory(selectedCategory)))

  let categories = null

  if (categoriesFetched !== null) {
    categories = categoriesFetched.map(category => {
      return <Link key={category.slug} to={`/restaurants/${category.slug}`}>{category.displayValue}</Link>
    })
  }

  return (
    <footer>
      <div>
        {/* <Link to="/">
          <Logo color="#57806C" width="200" />
        </Link> */}
        <Link to='/qui-sommes-nous'>À propos de Gogogood</Link>
        <a href='https://instagram.com/go_go_good_/' target='_blank' rel='noopener noreferrer'><FontAwesomeIcon icon={faInstagram} /></a>
        <a href='mailto:lea.de.saint.pierre@gmail.com'><FontAwesomeIcon icon={faEnvelope} /></a>
      </div>
      <div className={style.Links}>
        {/* <Link to="/">À propos de Gogogood</Link> */}
        <Link to='/restaurants/tokyo' onClick={() => onUpdateSelectedInput({ slug: 'tokyo', displayValue: 'Tokyo', extension: null })}>
          <div />
          <span>Tokyo Édition</span>
        </Link>
      </div>
      <div className={style.Districts}>
        {categories}
      </div>
    </footer>
  )
}

export default Footer
