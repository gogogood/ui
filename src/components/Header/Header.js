import React from 'react'
import { Link } from 'react-router-dom'
import Logo from '../../components/UI/Logo/Logo'
import LogoSmall from '../../components/UI/LogoSmall/LogoSmall'

import style from './Header.css'
import SelectInput from '../SelectInput/SelectInput'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram } from '@fortawesome/free-brands-svg-icons'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'

const Header = props => {
  let logo = (
    <Link to='/'>
      <Logo size='55' />
    </Link>
  )

  if (window.innerWidth < 1000) {
    logo = (
      <Link to='/'>
        <LogoSmall size='55' />
      </Link>
    )
  }
  if (window.innerWidth < 600) {
    logo = null
  }

  return (
    <header>
      {logo}
      <div className={style.Nav}>
        <Link to='/qui-sommes-nous'>À propos de Gogogood</Link>
        <a href='https://instagram.com/go_go_good_/' target='_blank' rel='noopener noreferrer'><FontAwesomeIcon icon={faInstagram} /></a>
        <a href='mailto:lea.de.saint.pierre@gmail.com'><FontAwesomeIcon icon={faEnvelope} /></a>
        <div>
          <SelectInput type='GreenWhiteHov' />
        </div>
      </div>
    </header>
  )
}

export default Header
