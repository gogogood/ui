import React from 'react'

import style from './LogoSmall.css'

const LogoSmall = props => {
  return <div style={{ fontSize: `${props.size}px` }} className={style.logoFont}>Ggg</div>
}

export default LogoSmall
