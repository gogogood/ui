import React from 'react'
import style from './Button.css'

const button = (props) => (
  <button
    className={[style.Button, style[props.type]].join(' ')}
    onClick={props.handleClicked}
  >{props.children}
  </button>
)

export default button
