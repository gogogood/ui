import React from 'react'

import style from './Logo.css'

// 400 // 118.92

const Logo = props => {
  return <div style={{ fontSize: `${props.size}px` }} className={style.logoFont}>Gogogood</div>
}

export default Logo
