import React from 'react'

import style from './Backdrop.css'

const backdrop = (props) => (
  props.show ? <div className={style.Backdrop} onClick={props.handleClosed} /> : null
)

export default backdrop
