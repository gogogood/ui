import React, { useCallback } from 'react'

import { useDispatch, useSelector } from 'react-redux'
import * as allActions from '../../../store/actions'

import { withRouter } from 'react-router-dom'

import style from './Input.css'

const Input = props => {
  const formConfig = useSelector(state => state.createRestaurant.formData)

  const dispatch = useDispatch()

  const onUpdateFormValue = useCallback((value) => dispatch(allActions.updateFormValue(value)))

  let inputElt = null
  const inputClasses = [style[props.type]]

  if (props.invalid && props.shouldValidate && props.touched) {
    inputClasses.push(style.Invalid)
  }

  const label = props.label ? <label className={style.Label}>{props.label}</label> : null

  const uploadWidget = (inputIdentifier) => {
    window.cloudinary.openUploadWidget({ cloud_name: 'dyywnpzbg', upload_preset: 'asb2ztu7' },
      (result) => {
        const updatedOrderForm = {
          ...formConfig
        }
        const updatedFormElement = {
          ...updatedOrderForm[inputIdentifier]
        }

        if (result) {
          updatedFormElement.value = result[0].url
          updatedFormElement.inputIdentifier = inputIdentifier
          updatedFormElement.valid = true
          updatedFormElement.touched = true
          updatedOrderForm[inputIdentifier] = updatedFormElement
          onUpdateFormValue(updatedFormElement)
        }
      })
  }

  switch (props.elementType) {
    case ('input'):
      inputElt = (
        <>
          {label}
          <input
            className={inputClasses.join(' ')}
            {...props.elementConfig}
            value={props.value}
            onChange={props.handleChanged}
          />
        </>
      )
      break
    case ('textarea'):
      inputElt = (
        <>
          {label}
          <textarea
            className={inputClasses.join(' ')}
            {...props.elementConfig}
            value={props.value}
            onChange={props.handleChanged}
          />
        </>
      )
      break
    case ('select'):
      inputElt = (
        <>
          {label}
          <div className={style.SelectPerso}>
            <select
              className={inputClasses.join(' ')}
              value={props.value}
              onChange={props.handleChanged}
            >
              {props.elementConfig.options.map(option => (
                <option key={option.slug} value={option.slug}>
                  {option.displayValue}
                </option>
              ))}
            </select>
          </div>
        </>
      )
      break
    case ('select-perso'):
      inputElt = (
        <>
          <div className={style.SelectPerso}>
            <select
              className={inputClasses.join(' ')}
              value={props.value}
              onChange={props.handleChanged}
            >
              {props.elementConfig.options.map(option => (
                <option key={option.slug} value={option.slug}>
                  {option.displayValue}{option.extension}
                </option>
              ))}
            </select>
          </div>
        </>
      )
      break
    case ('download'):
      inputElt = (
        <>
          {label}
          <div id='photo-form-container' style={{ marginBottom: '32px' }}>
            <div className={style.uploadBtn} onClick={() => uploadWidget(props.id)}>Upload Photo</div>
          </div>
          <input
            className={inputClasses.join(' ')}
            {...props.elementConfig}
            value={props.value}
            onChange={props.handleChanged}
          />
        </>
      )
      break
    default:
      inputElt = (
        <>
          {label}
          <input
            className={inputClasses.join(' ')}
            {...props.elementConfig}
            value={props.value}
            onChange={props.handleChanged}
          />
        </>
      )
  }

  return (
    <>
      {inputElt}
    </>
  )
}

export default withRouter(Input)
