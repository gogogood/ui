import axios from 'axios'

const instance = axios.create({
  // baseURL: 'https://localhost:8080'
  // baseURL: 'https://api-gogogood.herokuapp.com'
  baseURL: 'https://api.gogogood.fr'
})

export default instance
