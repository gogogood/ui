import React from 'react'

import Aux from '../Aux/Aux'
import Modal from '../../components/UI/Modal/Modal'
import useHttpErrorHandler from '../../hooks/httpErrorHandler'

const withErrorHandler = (WrappedComponent, api) => {
  return props => {
    const [error, clearError] = useHttpErrorHandler(api)

    return (
      <Aux>
        <Modal
          show={error}
          modalClosed={clearError}
        >
          {error ? error.message : null}
        </Modal>
        <WrappedComponent {...props} />
      </Aux>
    )
  }
}

export default withErrorHandler
