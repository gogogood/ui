export const form = {
  name: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: 'La Nonna',
      label: 'Nom du restaurant'
    },
    value: '',
    validation: {
      maxLength: 16,
      required: true
    },
    valid: false,
    touched: false
  },
  street: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: '25 avenue Théophile Gauthier',
      label: 'Adresse'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false
  },
  district: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: '75016',
      label: 'Code Postal'
    },
    value: '',
    validation: {
      required: true,
      minLength: 5,
      maxLength: 5
    },
    valid: false,
    touched: false
  },
  city: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: 'Paris',
      label: 'Ville'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false
  },
  country: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: 'France',
      label: 'France'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false
  },
  googleMapUrl: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: 'https://goo.gl/maps/6XDSyTTNQkKbpVY96',
      label: 'URL Google Maps'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false
  },
  phone: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: '+33 6 76 87 87 76',
      label: 'Téléphone'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false
  },
  openningHours: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: 'Ouvert tous les jours de 12h à 15h et de 19h à 23h, sauf le dimanche',
      label: 'Heures d\'ouvertures'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false
  },
  category: {
    elementType: 'select',
    elementConfig: {
      options: [],
      label: 'Sélectionnez une catégorie'
    },
    validation: {},
    value: '1'
  },
  catchPhrase: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: 'La nonna qui envoie du fat ! 🍅🍅 Nonna, dej et diner',
      label: 'Phrase d\'accroche'
    },
    value: 'valeur test kfnerln flejrfn  fknelrnfljr',
    validation: {
      required: true,
      maxLength: 63,
      minLength: 35
    },
    valid: false,
    touched: false
  },
  description: {
    elementType: 'textarea',
    elementConfig: {
      type: 'text',
      placeholder: 'Une description',
      label: 'Description'
    },
    value: '',
    validation: {
      required: true,
      maxLength: 791
    },
    valid: false,
    touched: false
  },
  plateName1: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: 'Pizza blanche au romarin',
      label: 'Plat N°1'
    },
    value: '',
    validation: {
      required: true,
      maxLength: 23
    },
    valid: false,
    touched: false
  },
  platePrice1: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: '12 €'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false
  },
  plateDescription1: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: 'Pizza sèche avec ses buratta, tranches de jambon et salade de roquette'
    },
    value: '',
    validation: {
      required: true,
      maxLength: 79
    },
    valid: false,
    touched: false
  },
  plateName2: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: 'Pizza blanche au romarin',
      label: 'Plat N°2'
    },
    value: '',
    validation: {
      required: false,
      maxLength: 23
    },
    valid: false,
    touched: false
  },
  platePrice2: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: '12 €'
    },
    value: '',
    validation: {
      required: false
    },
    valid: false,
    touched: false
  },
  plateDescription2: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: 'Pizza sèche avec ses buratta, tranches de jambon et salade de roquette'
    },
    value: '',
    validation: {
      required: false,
      maxLength: 79
    },
    valid: false,
    touched: false
  },
  plateName3: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: 'Pizza blanche au romarin',
      label: 'Plat N°3'
    },
    value: '',
    validation: {
      required: false,
      maxLength: 23
    },
    valid: false,
    touched: false
  },
  platePrice3: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: '12 €'
    },
    value: '',
    validation: {
      required: false
    },
    valid: false,
    touched: false
  },
  plateDescription3: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: 'Pizza sèche avec ses buratta, tranches de jambon et salade de roquette'
    },
    value: '',
    validation: {
      required: false,
      maxLength: 79
    },
    valid: false,
    touched: false
  },
  rate: {
    elementType: 'input',
    elementConfig: {
      type: 'text',
      placeholder: '5',
      label: 'Notation'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false
  },
  photoPola: {
    elementType: 'download',
    elementConfig: {
      type: 'text',
      placeholder: 'Lien vers la photo souhaitée dans le CDN Instagram',
      label: 'Photo du Polaroid dans la page de tous les restaurants'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false
  },
  Photo1: {
    elementType: 'download',
    elementConfig: {
      type: 'text',
      placeholder: 'Lien vers la photo souhaitée dans le CDN Instagram',
      label: 'Lien vers la photo souhaitée dans le CDN Instagram'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false
  },
  Photo2: {
    elementType: 'download',
    elementConfig: {
      type: 'text',
      placeholder: 'Lien vers la photo souhaitée dans le CDN Instagram',
      label: 'Lien vers la photo souhaitée dans le CDN Instagram'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false
  },
  Photo3: {
    elementType: 'download',
    elementConfig: {
      type: 'text',
      placeholder: 'Lien vers la photo souhaitée dans le CDN Instagram (Lieu)',
      label: 'Lien vers la photo souhaitée dans le CDN Instagram'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false
  }
}

export default form
