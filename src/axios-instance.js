import axios from 'axios'

const instance = axios.create({
  // baseURL: 'https://burger-builder-d2d1b.firebaseio.com/'
})

export default instance
