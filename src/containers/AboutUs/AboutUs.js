import React from 'react'

import MetaTags from 'react-meta-tags'

import Header from '../../components/Header/Header'
import Footer from '../../components/Footer/Footer'

import style from './AboutUs.css'

const AboutUs = () => {
  return (
    <>
      <MetaTags>
        {/* <title>Gogogood | {displayValue}</title>
        <meta name="description" content={'Tous les restaurants sélectionnés par Gogogood à ' + displayValue} />
        <meta property="og:title" content={'Gogogood | ' + displayValue} /> */}
      </MetaTags>
      <Header />
      <main className={style.MainAboutUs}>
        <h2>Qui sommes-nous ?</h2>
      </main>
      <Footer />
    </>
  )
}

export default AboutUs
