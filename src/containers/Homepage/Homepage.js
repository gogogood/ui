import React, { useCallback } from 'react'
import MetaTags from 'react-meta-tags'

import { useDispatch } from 'react-redux'
import * as restaurantActions from '../../store/actions'

import SelectInput from '../../components/SelectInput/SelectInput'
import Logo from '../../components/UI/Logo/Logo'
import Footer from '../../components/Footer/Footer'

import style from './Homepage.css'

import {
  Wrapper,
  LeftCutImg,
  LeftVerticalImg,
  MddlBox,
  MddlTopHoriImg,
  MddlBottomBox,
  MddlBttmLeftWrap,
  MddlBttmLeftBottomImg,
  MddlBttmLeftVerticalImg,
  RightCutImg
} from './HomepageStyle'

const Homepage = props => {
  const dispatch = useDispatch()

  const onUpdateSelectedInput = useCallback((selectedCategory) => dispatch(restaurantActions.updateSelectedCategory(selectedCategory)))

  let footer = null

  if (window.innerWidth < 1024) {
    footer = <Footer />
  }

  const onGetFocusHandler = () => {
    onUpdateSelectedInput({ category: 'lisbonne', displayValue: 'Portugal - Lisbonne' })
    props.history.push('/restaurants/lisbonne')
  }

  return (
    <>
      <MetaTags>
        <title>Gogogood | Y a pas moyen, GGG est là !</title>
        <meta name='description' content="Les moyens tu n'as pas, Gogogood est là !" />
        <meta property='og:title' content='Gogogood | Y a pas moyen, GGG est là !' />
      </MetaTags>
      <Wrapper>
        <LeftCutImg />
        <MddlBox>
          <MddlTopHoriImg />
          <MddlBottomBox>
            <MddlBttmLeftVerticalImg />
            <MddlBttmLeftWrap>
              <div className={style.CtaWrapperHome}>
                <div className={style.SvgBox}>
                  <Logo size='100' />
                </div>
                <p>Les moyens tu n'as pas,<br />des bons plans on t'apportera !</p>
                <div>
                  <SelectInput type='WhiteGreenHov' />
                </div>
              </div>
              <MddlBttmLeftBottomImg />
            </MddlBttmLeftWrap>
          </MddlBottomBox>
        </MddlBox>
        <LeftVerticalImg onClick={onGetFocusHandler} />
        <RightCutImg />
      </Wrapper>
      {footer}
    </>
  )
}

export default Homepage
