import styled from 'styled-components'
import { device } from '../../device'

const imageLeftCutImg = 'https://res.cloudinary.com/dyywnpzbg/image/upload/v1572445789/72158211_710892642727383_8997374942253154304_n_ntgxp0.jpg'
const imageLeftVerticalImg = 'https://res.cloudinary.com/dyywnpzbg/image/upload/v1572445788/71802377_3386140851403885_6170984057132810240_n_pbzbw8.jpg'
const imageMddlTopHoriImg = 'https://res.cloudinary.com/dyywnpzbg/image/upload/v1572445789/71676489_491472525040297_4039665003882610688_n_d7jugy.jpg'
const imageMddlBttmLeftBottomImg = 'https://res.cloudinary.com/dyywnpzbg/image/upload/v1572445788/72322241_2568504129910154_8889848301399048192_n_vi8psd.jpg'
const imageMddlBttmLeftVerticalImg = 'https://res.cloudinary.com/dyywnpzbg/image/upload/v1572445788/71699732_2560865803964365_1625139543755718656_n_zkdxbq.jpg'
const imageRightCutImg = 'https://res.cloudinary.com/dyywnpzbg/image/upload/v1572445789/72158211_710892642727383_8997374942253154304_n_ntgxp0.jpg'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #57806C;
  
  @media ${device.laptop} {
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: space-between; 
    flex-direction: row;
  }
`

export const LeftCutImg = styled.div`
  @media ${device.laptop} {
    border-top: 16px solid #57806C;
    border-right: 8px solid #57806C;
    border-bottom: 16px solid #57806C;
    border-left: 0;
    flex: 0 1 10%;
    background-image: url("${imageLeftCutImg}");
    background-color: #BBE2D0;
    padding: 16px;
    background-position: left;
  }
`

export const LeftVerticalImg = styled.div`
  background-image: url("${imageLeftVerticalImg}");
  background-color: #BBE2D0
  border: 16px solid white;
  width: 90%;
  margin: 64px auto;
  height: 250px;
  background-size: cover;
  // background-position-x: 100%;
  background-position: center;
  cursor: pointer;
  font-size: 20px;
  color: black;
  font-weight: 600;
  display: flex; 
  align-items: center;
  justify-content: space-around;   
  transition: 0.4s ease color;
  position: relative;

  @media ${device.laptop} {
    border-top: 16px solid #57806C;
    border-right: 8px solid #57806C;
    border-bottom: 16px solid #57806C;
    border-left: 8px solid #57806C;
    flex: 0 0 23%;
    width: auto;
    height: auto;
    margin: 0;
    align-items: flex-end;
    color: white;
    padding-bottom: 128px; 
    position: relative;
  }
`

export const MddlBox = styled.div`

  @media ${device.laptop} {
    border-top: 16px solid #57806C;
    border-right: 8px solid #57806C;
    border-bottom: 16px solid #57806C;
    border-left: 8px solid #57806C;
    flex: 0 0 57%;
    display: flex;
    flex-direction: column; 
  }
`

export const MddlTopHoriImg = styled.div`
  background-image: url("${imageMddlTopHoriImg}");
  background-color: #BBE2D0
  background-size: cover;
  background-position: center;
  width: 100%;
  height: 150px;
  background-position-y: 30%;
  @media ${device.laptop} {
    flex: 0 0 24%;
    width: auto;
    height: auto;
  }
`

export const MddlBottomBox = styled.div`

  @media ${device.laptop} {
    flex: 0 0 76%;
    display: flex;
    justify-content: space-between;
  }
`

export const MddlBttmLeftWrap = styled.div`

  @media ${device.laptop} {
    border-top: 16px solid #57806C;
    border-left: 8px solid #57806C;
    flex: 0 1 59%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    background-color: #57806C;
  }
`

export const MddlBttmLeftBottomImg = styled.div`

  @media ${device.laptop} {
    flex: 0 0 32%;
    background-size: cover;
    background-position: center;
    background-image: url("${imageMddlBttmLeftBottomImg}");
    background-color: #BBE2D0;
  }
`

export const MddlBttmLeftVerticalImg = styled.div`

  @media ${device.laptop} {
    border-top: 16px solid #57806C;
    border-right: 8px solid #57806C;
    flex: 0 1 41%;
    background-position: center;
    background-image: url("${imageMddlBttmLeftVerticalImg}");
    background-color: #BBE2D0;
  }
`

export const RightCutImg = styled.div`

  @media ${device.laptop} {
    border-top: 16px solid #57806C;
    border-right: 0;
    border-bottom: 16px solid #57806C;
    border-left: 8px solid #57806C;
    flex: 0 1 10%;
    background-image: url("${imageRightCutImg}");
    background-color: #BBE2D0;
    background-position: right;
  }
`
