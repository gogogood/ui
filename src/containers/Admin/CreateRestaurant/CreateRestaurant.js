/* global localStorage */

import React, { useState, useEffect, useCallback } from 'react'

import { useDispatch, useSelector } from 'react-redux'
import * as allActions from '../../../store/actions'

import { withRouter } from 'react-router-dom'

import api from '../../../axios-api'

import Input from '../../../components/UI/Input/Input'
import Button from '../../../components/UI/Button/Button'

import ErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler'

const CreateRestaurant = props => {
  const [error, setError] = useState(false)

  const isSignedIn = useSelector(state => state.auth.token !== null)

  const dispatch = useDispatch()
  const onSignInSuccess = useCallback((token, localId, expirationDate) => dispatch(allActions.signInSuccess(token, localId, expirationDate)))

  useEffect(() => {
    if (!isSignedIn) {
      const token = localStorage.getItem('token')
      const localId = localStorage.getItem('localId')
      const expirationDate = localStorage.getItem('Expires at')

      if (token !== null) {
        if (new Date(expirationDate) > new Date()) {
          onSignInSuccess(token, localId, expirationDate)
        } else {
          props.history.replace('/administration/login')
        }
      }
    }
  }, [isSignedIn])

  const {
    fetchCategoriesStart,
    fetchCategoriesSuccess,
    fetchCategoriesFail
  } = props

  const formConfig = useSelector(state => state.createRestaurant.formData)

  const onUpdateFormValue = useCallback((value) => dispatch(allActions.updateFormValue(value)))
  const onfetchCategoriesStart = () => dispatch(allActions.fetchCategoriesStart())
  const onfetchCategoriesSuccess = useCallback((opts) => dispatch(allActions.fetchCategoriesSuccess(opts)), [dispatch])
  const onfetchCategoriesFail = () => dispatch(allActions.fetchCategoriesFail())

  useEffect(() => {
    onfetchCategoriesStart()
    api.get('/categories/all')
      .then(response => {
        onfetchCategoriesSuccess(response.data.categories)
      })
      .catch(error => {
        console.log(error)
        onfetchCategoriesFail()
      })
  }, [fetchCategoriesStart, fetchCategoriesSuccess, fetchCategoriesFail])

  const orderHandler = (event) => {
    event.preventDefault()

    const restaurantData = {}
    for (const formElementIdentifier in formConfig) {
      restaurantData[formElementIdentifier] = formConfig[formElementIdentifier].value
    }
    restaurantData.creationDate = new Date()
    api.post('/posts/create', restaurantData)
      .then(response => {
        if (response.status !== 422) {
          props.history.replace('/administration/')
        }
      })
      .catch(error => {
        setError(error)
        console.log(error)
      })
  }

  const checkValidity = (value, rules) => {
    let isValid = true

    if (rules.required) {
      isValid = value.trim() !== '' && isValid
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.minLength && isValid
    }

    return isValid
  }

  const inputChangedHandler = (event, inputIdentifier) => {
    const updatedOrderForm = {
      ...formConfig
    }
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    }

    updatedFormElement.value = event.target.value
    updatedFormElement.inputIdentifier = inputIdentifier
    updatedFormElement.valid = checkValidity(updatedFormElement.value, updatedFormElement.validation)
    updatedFormElement.touched = true
    updatedOrderForm[inputIdentifier] = updatedFormElement
    onUpdateFormValue(updatedFormElement)
  }

  const formElementArray = []
  for (const key in formConfig) {
    formElementArray.push({
      _id: key,
      config: formConfig[key]
    })
  }

  const errorHandler = () => {
    setError(false)
  }

  const inputs = formElementArray.map((formElement) => {
    if (formElement.config.elementType === 'select') {
      return (
        <div key={formElement._id} style={{ marginBottom: '32px' }}>
          <Input
            key={formElement._id}
            id={formElement._id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            label={formElement.config.elementConfig.label}
            value={formElement.config.value}
            invalid={!formElement.config.valid}
            shouldValidate={formElement.config.validation}
            touched={formElement.config.touched}
            handleChanged={(event) => inputChangedHandler(event, formElement._id)}
          />
        </div>
      )
    }

    if (formElement.config.elementType === 'download') {
      return (
        <Input
          key={formElement._id}
          id={formElement._id}
          elementType={formElement.config.elementType}
          elementConfig={formElement.config.elementConfig}
          label={formElement.config.elementConfig.label}
          value={formElement.config.value}
          invalid={!formElement.config.valid}
          shouldValidate={formElement.config.validation}
          touched={formElement.config.touched}
          handleChanged={(event) => inputChangedHandler(event, formElement._id)}
        />
      )
    }

    return (
      <Input
        key={formElement._id}
        id={formElement._id}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        label={formElement.config.elementConfig.label}
        value={formElement.config.value}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        handleChanged={(event) => inputChangedHandler(event, formElement._id)}
      />
    )
  })

  let form = (
    <form onSubmit={orderHandler}>
      {inputs}
      <Button type='ButtonInfo'>Create restaurant</Button>
    </form>
  )

  if (props.loading) {
    form = <p>Loading...</p>
  }

  return (
    <div style={{ width: '50%' }}>
      <ErrorHandler error={error} onHandle={errorHandler} />
      <h1>Create a restaurant</h1>
      {form}
    </div>
  )
}

export default withRouter(ErrorHandler(CreateRestaurant, api))
