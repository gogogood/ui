/* global localStorage */

import React, { useState, useEffect, useCallback } from 'react'

import { useDispatch, useSelector } from 'react-redux'
import * as allActions from '../../../store/actions'

import api from '../../../axios-api'

import Input from '../../../components/UI/Input/Input'
import Button from '../../../components/UI/Button/Button'

import ErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler'

const CreateCategory = props => {
  const [error, setError] = useState(false)

  const isSignedIn = useSelector(state => state.auth.token !== null)

  const dispatch = useDispatch()
  const onSignInSuccess = useCallback((token, localId, expirationDate) => dispatch(allActions.signInSuccess(token, localId, expirationDate)))

  useEffect(() => {
    if (!isSignedIn) {
      const token = localStorage.getItem('token')
      const localId = localStorage.getItem('localId')
      const expirationDate = localStorage.getItem('Expires at')

      if (token !== null) {
        if (new Date(expirationDate) > new Date()) {
          onSignInSuccess(token, localId, expirationDate)
        } else {
          props.history.replace('/administration/login')
        }
      }
    }
  }, [isSignedIn])

  const [orderForm, setOrderForm] = useState({
    displayValue: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Display Value',
        label: 'Valeur d\'affichage (Paris 1er pour Paris, le nom de la ville pour un pays)'
      },
      value: '',
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    extension: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'ème, er, etc...',
        label: 'Extension de l\'arrondissement'
      },
      value: '',
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    slug: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'minuscule, en un mot ou séparé par des -',
        label: 'Identifiant'
      },
      value: '',
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  })

  const orderHandler = (e) => {
    e.preventDefault()
    const categoryData = {}
    for (const formElementIdentifier in orderForm) {
      categoryData[formElementIdentifier] = orderForm[formElementIdentifier].value
    }

    api.post('/categories/create', categoryData)
      .then(response => {
        if (response.status !== 422) {
          props.history.push('/administration')
        }
      })
      .catch(error => {
        console.log(error)
        setError(error)
      })
  }

  const checkValidity = (value, rules) => {
    let isValid = true

    if (rules.required) {
      isValid = value.trim() !== '' && isValid
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.minLength && isValid
    }

    return isValid
  }

  const inputChangedHandler = (event, inputIdentifier) => {
    const updatedOrderForm = {
      ...orderForm
    }
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    }
    updatedFormElement.value = event.target.value
    updatedFormElement.valid = checkValidity(updatedFormElement.value, updatedFormElement.validation)
    updatedFormElement.touched = true
    updatedOrderForm[inputIdentifier] = updatedFormElement
    setOrderForm(updatedOrderForm)
  }

  const errorHandler = () => {
    setError(false)
  }

  const formElementArray = []
  for (const key in orderForm) {
    formElementArray.push({
      _id: key,
      config: orderForm[key]
    })
  }

  const inputs = formElementArray.map((formElement) => {
    if (formElement.config.elementType === 'select') {
      return (
        <div style={{ marginBottom: '32px' }}>
          <Input
            key={formElement._id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            label={formElement.config.elementConfig.label}
            value={formElement.config.value}
            invalid={!formElement.config.valid}
            shouldValidate={formElement.config.validation}
            touched={formElement.config.touched}
            handleChanged={(event) => inputChangedHandler(event, formElement._id)}
          />
        </div>
      )
    } else {
      return (
        <Input
          key={formElement._id}
          elementType={formElement.config.elementType}
          elementConfig={formElement.config.elementConfig}
          label={formElement.config.elementConfig.label}
          value={formElement.config.value}
          invalid={!formElement.config.valid}
          shouldValidate={formElement.config.validation}
          touched={formElement.config.touched}
          handleChanged={(event) => inputChangedHandler(event, formElement._id)}
        />
      )
    }
  })

  let form = (
    <form onSubmit={orderHandler}>
      {inputs}
      <Button type='ButtonInfo'>Create category</Button>
    </form>
  )

  if (props.loading) {
    form = <p>Loading...</p>
  }
  return (
    <div style={{ width: '40%' }}>
      <ErrorHandler error={error} onHandle={errorHandler} />
      <h1>Create a category</h1>
      {form}
    </div>
  )
}

export default ErrorHandler(CreateCategory, api)
