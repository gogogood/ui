/* global localStorage */

import React, { useCallback } from 'react'

import { useDispatch, useSelector } from 'react-redux'
import * as allActions from '../../../store/actions'

import axios from 'axios'

import Input from '../../../components/UI/Input/Input'
import Button from '../../../components/UI/Button/Button'
import Loader from '../../../components/UI/Loader/Loader'

const Auth = props => {
  const formConfig = useSelector(state => state.auth.formData)
  const loading = useSelector(state => state.auth.loading)

  const dispatch = useDispatch()

  const onUpdateAuthFormValue = useCallback((value) => dispatch(allActions.updateAuthFormValue(value)))
  const onSignInStart = () => dispatch(allActions.signInStart())
  const onSignInSuccess = useCallback((token, localId, expirationDate) => dispatch(allActions.signInSuccess(token, localId, expirationDate)))
  const onSignInFail = () => dispatch(allActions.signInFail())

  const orderHandler = (event) => {
    event.preventDefault()
    const authData = {}
    for (const formElementIdentifier in formConfig) {
      authData[formElementIdentifier] = formConfig[formElementIdentifier].value
    }
    authData.returnSecureToken = true
    onSignInStart()
    axios.post('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBFI2TDb-TSj4Ez1pheQSIsaZoXsCk55I0', authData)
      .then(response => {
        onSignInSuccess(response.data.idToken, response.data.localId, response.data.expiresIn)
        const expirationDate = new Date(new Date().getTime() + +response.data.expiresIn * 1000)
        localStorage.setItem('token', response.data.idToken)
        localStorage.setItem('localId', response.data.localId)
        localStorage.setItem('Expires at', expirationDate)
        props.history.push('/administration/index-restaurant')
      })
      .catch(error => {
        console.log(error)
        onSignInFail()
      })
  }

  const checkValidity = (value, rules) => {
    let isValid = true

    if (rules.required) {
      isValid = value.trim() !== '' && isValid
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.minLength && isValid
    }

    return isValid
  }

  const inputChangedHandler = (event, inputIdentifier) => {
    const updatedOrderForm = {
      ...formConfig
    }
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    }

    updatedFormElement.value = event.target.value
    updatedFormElement.inputIdentifier = inputIdentifier
    updatedFormElement.valid = checkValidity(updatedFormElement.value, updatedFormElement.validation)
    updatedFormElement.touched = true
    updatedOrderForm[inputIdentifier] = updatedFormElement
    onUpdateAuthFormValue(updatedFormElement)
  }

  const formElementArray = []
  for (const key in formConfig) {
    formElementArray.push({
      _id: key,
      config: formConfig[key]
    })
  }

  const inputs = formElementArray.map((formElement) => {
    return (
      <Input
        key={formElement._id}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        label={formElement.config.elementConfig.label}
        value={formElement.config.value}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        handleChanged={(event) => inputChangedHandler(event, formElement._id)}
      />
    )
  })

  const button = <Button type='ButtonInfo'>Se connecter</Button>

  const form = (
    <form onSubmit={orderHandler}>
      {inputs}
      {button}
    </form>
  )

  let login = (
    <div style={{ width: '40%' }}>
      <h1 style={{ fontWeight: '600' }}>Se connecter à l'admin</h1>
      {form}
    </div>
  )

  if (loading) {
    login = <div style={{ display: 'flex', justifyContent: 'space-around', alignItems: 'center' }}><Loader width='200' />  </div>
  }

  return login
}

export default Auth
