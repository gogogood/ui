/* global localStorage */

import React, { useEffect, useCallback } from 'react'
import { Route, withRouter } from 'react-router-dom'

import { useDispatch, useSelector } from 'react-redux'
import * as allActions from '../../store/actions'

import Button from '../../components/UI/Button/Button'
import CreateRestaurant from './CreateRestaurant/CreateRestaurant'
import UpdateRestaurant from './UpdateRestaurant/UpdateRestaurant'
import CreateCategory from './CreateCategory/CreateCategory'
import IndexRestaurants from './IndexRestaurants/IndexRestaurants'
import Auth from './Auth/Auth'

import style from './Admin.css'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPowerOff } from '@fortawesome/free-solid-svg-icons'

const Admin = props => {
  const isSignedIn = useSelector(state => state.auth.token !== null)

  const dispatch = useDispatch()

  const onSignInSuccess = useCallback((token, localId, expirationDate) => dispatch(allActions.signInSuccess(token, localId, expirationDate)))
  const onLogOutSuccess = () => dispatch(allActions.logOutSuccess())
  const onEmptyRestaurantState = () => dispatch(allActions.emptyRestaurantState())
  const onEmptyRestaurantForm = useCallback(() => dispatch(allActions.emptyRestaurantForm()))

  useEffect(() => {
    if (!isSignedIn) {
      const token = localStorage.getItem('token')
      const localId = localStorage.getItem('localId')
      const expirationDate = localStorage.getItem('Expires at')

      if (token !== null) {
        if (new Date(expirationDate) > new Date()) {
          onSignInSuccess(token, localId, expirationDate)
        } else {
          props.history.replace('/administration/login')
        }
      }
    }
  }, [isSignedIn])

  const onCreateCategoryHandler = () => {
    props.history.push('/administration/create-category')
  }

  const onCreateRestaurantHandler = () => {
    onEmptyRestaurantForm()
    props.history.push('/administration/create-restaurant')
  }

  const onGetRestaurantsHandler = () => {
    props.history.push('/administration/index-restaurant')
  }

  const onGetLoginHandler = () => {
    props.history.push('/administration/login')
  }

  const onGetHomepageHandler = () => {
    onEmptyRestaurantState()
    props.history.replace('/')
  }

  const onLogoutOut = () => {
    localStorage.removeItem('token')
    localStorage.removeItem('localId')
    localStorage.removeItem('Expires at')
    onLogOutSuccess()
    props.history.replace('/administration')
  }

  let adminActions = (
    <div>
      <Button handleClicked={onGetRestaurantsHandler} type='ButtonInfo'>All restaurants</Button>
      <Button handleClicked={onCreateCategoryHandler} type='ButtonInfo'>New category</Button>
      <Button handleClicked={onCreateRestaurantHandler} type='ButtonInfo'>New restaurant</Button>
    </div>
  )

  if (!isSignedIn) {
    adminActions = null
  }

  let signInButton = <Button handleClicked={onGetLoginHandler} type='ButtonInfo'>Se Connecter</Button>

  if (isSignedIn) {
    signInButton = <Button handleClicked={onLogoutOut} type='ButtonInfo'><FontAwesomeIcon icon={faPowerOff} /></Button>
  }

  return (
    <main className={style.MainAdmin}>
      <div className={style.WrapperAdmin}>
        <h1 style={{ fontWeight: '600' }}>Admin</h1>
        <div className={style.Actions}>
          {adminActions}
          <div>
            <Button clicked={onGetHomepageHandler} type='ButtonInfo' style={{ alignSelf: 'end' }}>Go to homepage</Button>
            {signInButton}
          </div>
        </div>
        <Route path='/administration/create-restaurant' component={CreateRestaurant} />
        <Route path='/administration/update-restaurant/:id' component={UpdateRestaurant} />
        <Route path='/administration/create-category' component={CreateCategory} />
        <Route path='/administration/index-restaurant' component={IndexRestaurants} />
        <Route path='/administration/login' component={Auth} />
      </div>
    </main>
  )
}

export default withRouter(Admin)
