/* global localStorage */

import React, { useEffect, useCallback } from 'react'

import { useDispatch, useSelector } from 'react-redux'
import * as allActions from '../../../store/actions'

import { Link } from 'react-router-dom'

import api from '../../../axios-api'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrashAlt, faEye } from '@fortawesome/free-solid-svg-icons'

import './IndexRestaurants.css'

const IndexRestaurants = props => {
  const {
    fetchRestaurantsStart,
    fetchRestaurantsSuccess,
    fetchRestaurantsFail
  } = props

  const isSignedIn = useSelector(state => state.auth.token !== null)
  const fetchedRestaurants = useSelector(state => state.fetchRestaurants.restaurants)

  const dispatch = useDispatch()
  const onSignInSuccess = useCallback((token, localId, expirationDate) => dispatch(allActions.signInSuccess(token, localId, expirationDate)))
  const onfetchRestaurantsStart = () => dispatch(allActions.fetchRestaurantsStart())
  const onfetchRestaurantsSuccess = useCallback((restaurants) => dispatch(allActions.fetchRestaurantsSuccess(restaurants)), [dispatch])
  const onfetchRestaurantsFail = () => dispatch(allActions.fetchRestaurantsFail())

  useEffect(() => {
    if (!isSignedIn) {
      const token = localStorage.getItem('token')
      const localId = localStorage.getItem('localId')
      const expirationDate = localStorage.getItem('Expires at')

      if (token !== null) {
        if (new Date(expirationDate) > new Date()) {
          onSignInSuccess(token, localId, expirationDate)
        } else {
          props.history.replace('/administration/login')
        }
      }
    } else {
      onfetchRestaurantsStart()
      api.get('/posts/all')
        .then(response => {
          console.log(response.data.posts)
          onfetchRestaurantsSuccess(response.data.restaurants)
        })
        .catch((error) => {
          console.log(error)
          onfetchRestaurantsFail()
        })
    }
  }, [
    isSignedIn,
    fetchRestaurantsStart,
    fetchRestaurantsSuccess,
    fetchRestaurantsFail])

  // const editingRestaurant = useSelector(state => state.createRestaurant.editingElement)
  const onupdateRestaurantStart = useCallback((restaurant) => dispatch(allActions.updateRestaurantStart(restaurant)))

  const onGetEditRestaurantHandler = (restaurant, id) => {
    restaurant.id = id
    onupdateRestaurantStart(restaurant)
    props.history.push('/administration/update-restaurant/' + id)
  }

  const onDeleteRestaurantHandler = (id) => {
    api.delete(`/posts/${id}`)
      .then(response => {
        console.log(response)
        // UPDATE THE STATE
      })
      .catch(error => {
        console.log(error)
      })
  }

  const restaurants = fetchedRestaurants.map((restaurant) => {
    return (
      <tr key={restaurant._id}>
        <td>{restaurant.name}</td>
        <td>{restaurant.category}</td>
        <td>{restaurant.street}, {restaurant.district} {restaurant.city}</td>
        <td>{restaurant.googleMapUrl}</td>
        <td>
          <FontAwesomeIcon onClick={() => onGetEditRestaurantHandler(restaurant, restaurant._id)} icon={faEdit} />
          <Link to={'/restaurants/' + restaurant.category + '/' + restaurant._id} target='_blank'><FontAwesomeIcon icon={faEye} /></Link>
          <FontAwesomeIcon onClick={() => onDeleteRestaurantHandler(restaurant._id)} icon={faTrashAlt} />
        </td>
      </tr>
    )
  })

  return (
    <>
      <h1>All restaurants</h1>
      <table>
        <thead>
          <tr>
            <th>Nom du restaurant</th>
            <th>Catégorie</th>
            <th>Adresse</th>
            <th>Lien Google Maps</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {restaurants}
        </tbody>
      </table>
    </>
  )
}

export default IndexRestaurants
