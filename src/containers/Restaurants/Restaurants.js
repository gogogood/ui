import React from 'react'

import { Route, Switch } from 'react-router-dom'

import Header from '../../components/Header/Header'
import Footer from '../../components/Footer/Footer'
import Restaurants from '../../components/Restaurants/Restaurants'
import Restaurant from '../../components/Restaurant/Restaurant'

const RestaurantsIndex = props => {
  return (
    <fragment>
      <Header />
      <Switch>
        <Route path='/restaurants/:district' component={Restaurants} />
        <Route path='/restaurants/:district/:id' component={Restaurant} />
      </Switch>
      <Footer />
    </fragment>
  )
}

export default RestaurantsIndex
