import React from 'react'
import { withRouter } from 'react-router-dom'

import Header from '../../../components/Header/Header'
import Footer from '../../../components/Footer/Footer'

import RestaurantView from '../../../components/Restaurant/Restaurant'

const Restaurant = props => {
  return (
    <fragment>
      <Header />
      <RestaurantView />
      <Footer />
    </fragment>
  )
}

export default withRouter(Restaurant)
