import React, { Suspense } from 'react'
import { Route, Switch, withRouter } from 'react-router-dom'

import { useSelector } from 'react-redux'

import Layout from './hoc/Layout/Layout'
import Loader from './components/UI/Loader/Loader'
import Homepage from './containers/Homepage/Homepage'
import ErrorPage from './components/ErrorPage/ErrorPage'

import ScrollToTop from './hooks/scrollToTop'

import style from './App.css'

const RestaurantsIndex = React.lazy(() => {
  return import('./containers/Restaurants/Restaurants')
})

const RestaurantView = React.lazy(() => {
  return import('./containers/Restaurants/Restaurant/Restaurant')
})

const AboutUsView = React.lazy(() => {
  return import('./containers/AboutUs/AboutUs')
})

const AdminView = React.lazy(() => {
  return import('./containers/Admin/Admin')
})

const CreateRestaurantView = React.lazy(() => {
  return import('./containers/Admin/CreateRestaurant/CreateRestaurant')
})

const UpdateRestaurantView = React.lazy(() => {
  return import('./containers/Admin/UpdateRestaurant/UpdateRestaurant')
})

const CreateCategoryView = React.lazy(() => {
  return import('./containers/Admin/CreateCategory/CreateCategory')
})

const IndexRestaurantsView = React.lazy(() => {
  return import('./containers/Admin/IndexRestaurants/IndexRestaurants')
})

const AuthView = React.lazy(() => {
  return import('./containers/Admin/Auth/Auth')
})

const App = props => {
  const isSignedIn = useSelector(state => state.auth.token !== null)

  let routes = (
    <>
      <ScrollToTop />
      <Switch>
        <Route path='/' exact component={Homepage} />
        <Route path='/restaurants/:district/' exact render={props => <RestaurantsIndex {...props} />} />
        <Route path='/restaurants/:district/:id/' exact render={props => <RestaurantView {...props} />} />
        <Route path='/qui-sommes-nous/' exact render={props => <AboutUsView {...props} />} />
        <Route path='/administration/' render={props => <AdminView {...props} />} />
        <Route path='/administration/login/' exact render={props => <AuthView {...props} />} />
        <Route render={props => <ErrorPage {...props} />} />
      </Switch>
    </>
  )

  if (isSignedIn) {
    routes = (
      <Switch>
        <Route path='/' exact component={Homepage} />
        <Route path='/restaurants/:district/' exact render={props => <RestaurantsIndex {...props} />} />
        <Route path='/restaurants/:district/:id/' exact render={props => <RestaurantView {...props} />} />
        <Route path='/qui-sommes-nous/' exact render={props => <RestaurantView {...props} />} />
        <Route path='/administration/' render={props => <AdminView {...props} />} />
        <Route path='/administration/create-restaurant/' exact render={props => <CreateRestaurantView {...props} />} />
        <Route path='/administration/update-restaurant/:id/' exact render={props => <UpdateRestaurantView {...props} />} />
        <Route path='/administration/create-category/' exact render={props => <CreateCategoryView {...props} />} />
        <Route path='/administration/index-restaurant/' exact render={props => <IndexRestaurantsView {...props} />} />
        <Route path='/administration/login/' exact render={props => <AuthView {...props} />} />
        <Route render={props => <ErrorPage {...props} />} />
      </Switch>
    )
  }

  return (
    <Layout>
      <Suspense
        fallback={<div className={style.LoaderContainer}><Loader width='400' /></div>}
      >
        {routes}
      </Suspense>
    </Layout>
  )
}

export default withRouter(App)
