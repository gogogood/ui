import * as actionTypes from './actionTypes'

export const fetchCategoriesStart = () => {
  return {
    type: actionTypes.FETCH_CATEGORIES_START
  }
}

export const fetchCategoriesSuccess = (options) => {
  return {
    type: actionTypes.FETCH_CATEGORIES_SUCCESS,
    options: options
  }
}

export const fetchCategoriesFail = () => {
  return {
    type: actionTypes.FETCH_CATEGORIES_FAIL
  }
}

export const updateFormValue = (value) => {
  return {
    type: actionTypes.UPDATE_FORM_VALUE,
    value: value
  }
}

export const fetchRestaurantsStart = () => {
  return {
    type: actionTypes.FETCH_RESTAURANTS_START
  }
}

export const fetchRestaurantsSuccess = (restaurants) => {
  return {
    type: actionTypes.FETCH_RESTAURANTS_SUCCESS,
    restaurants: restaurants
  }
}

export const fetchRestaurantsFail = () => {
  return {
    type: actionTypes.FETCH_RESTAURANTS_FAIL
  }
}

export const fetchCategoriesSelectInputStart = () => {
  return {
    type: actionTypes.FETCH_CATEGORIES_SELECT_INPUT_START
  }
}

export const fetchCategoriesSelectInputSuccess = (categories) => {
  return {
    type: actionTypes.FETCH_CATEGORIES_SELECT_INPUT_SUCCESS,
    categories: categories
  }
}

export const fetchCategoriesSelectInputFail = () => {
  return {
    type: actionTypes.FETCH_CATEGORIES_SELECT_INPUT_FAIL
  }
}

export const updateSelectedCategory = (selectedCategory) => {
  return {
    type: actionTypes.UPDATE_SELECTED_CATEGORY,
    selectedCategory: selectedCategory
  }
}

export const updateSelectedRestaurant = (restaurant) => {
  return {
    type: actionTypes.UPDATE_SELECTED_RESTAURANT,
    selectedRestaurant: restaurant
  }
}

export const updateRestaurants = (restaurant) => {
  return {
    type: actionTypes.UPDATE_RESTAURANTS,
    restaurant: restaurant
  }
}

export const updateRestaurantStart = (restaurant) => {
  return {
    type: actionTypes.UPDATE_RESTAURANT_START,
    restaurant: restaurant
  }
}

export const updateRestaurantSuccess = (restaurant) => {
  return {
    type: actionTypes.UPDATE_RESTAURANT_SUCCESS,
    restaurant: restaurant
  }
}

export const emptyRestaurantForm = () => {
  return {
    type: actionTypes.EMPTY_RESTAURANT_FORM
  }
}

export const emptyRestaurantState = () => {
  return {
    type: actionTypes.EMPTY_RESTAURANT_STATE
  }
}
