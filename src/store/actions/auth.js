import * as actionTypes from './actionTypes'

export const updateAuthFormValue = (value) => {
  return {
    type: actionTypes.UPDATE_AUTH_FORM_VALUE,
    value: value
  }
}

export const signInStart = () => {
  return {
    type: actionTypes.SIGN_IN_START
  }
}

export const signInSuccess = (token, localId, expiresIn) => {
  return {
    type: actionTypes.SIGN_IN_SUCCESS,
    token: token,
    localId: localId,
    expiresIn: expiresIn
  }
}
export const signInFail = () => {
  return {
    type: actionTypes.SIGN_IN_FAIL
  }
}

export const logOutSuccess = () => {
  return {
    type: actionTypes.LOG_OUT_SUCCESS
  }
}
