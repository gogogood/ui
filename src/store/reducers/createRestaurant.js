import * as actionTypes from '../actions/actionTypes'

import form from '../../data/form'

const initialState = {
  editingElement: null,
  edtEltCategory: null,
  formData: form,
  loading: false,
  categories: null
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_CATEGORIES_START:
      return {
        ...state,
        loading: true
      }
    case actionTypes.FETCH_CATEGORIES_SUCCESS:
      return {
        ...state,
        categories: action.options,
        loading: false,
        formData: {
          ...state.formData,
          category: {
            ...state.category,
            elementType: 'select',
            elementConfig: {
              ...state.elementConfig,
              options: action.options,
              label: 'Sélectionnez une catégorie'
            },
            validation: {},
            value: state.edtEltCategory !== null ? state.edtEltCategory : action.options[0].slug
          }
        }
      }
    case actionTypes.FETCH_CATEGORIES_FAIL:
      return {
        ...state,
        loading: false
      }
    case actionTypes.UPDATE_FORM_VALUE:
      return {
        ...state,
        loading: false,
        formData: {
          ...state.formData,
          [action.value.inputIdentifier]: {
            ...state[action.value.inputIdentifier],
            value: action.value.value,
            elementType: action.value.elementType,
            validation: action.value.validation,
            touched: action.value.touched,
            elementConfig: action.value.elementConfig
          }
        }
      }
    case actionTypes.UPDATE_RESTAURANT_START: {
      const _ = {
        ...state,
        formData: {
          ...state.formData
        }
      }
      for (const input in _.formData) {
        _.formData[input].value = action.restaurant[input]
      }
      return {
        ...state,
        editingElement: action.restaurant,
        formData: _.formData,
        edtEltCategory: action.restaurant.category
      }
    }
    case actionTypes.EMPTY_RESTAURANT_FORM: {
      const currentForm = {
        ...state,
        formData: {
          ...state.formData
        }
      }
      for (const input in currentForm.formData) {
        currentForm.formData[input].value = ''
      }
      return {
        ...state,
        formData: currentForm.formData
      }
    }
    default:
      return state
  }
}

export default reducer
