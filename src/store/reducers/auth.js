import * as actionTypes from '../actions/actionTypes'

const initialState = {
  token: null,
  localId: null,
  expirationDate: null,
  loading: false,
  formData: {
    email: {
      elementType: 'input',
      elementConfig: {
        type: 'email',
        placeholder: 'lola@gmail.com',
        label: 'Adresse Email'
      },
      value: '',
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    password: {
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: '********',
        label: 'Mot de passe'
      },
      value: '',
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  }
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.UPDATE_AUTH_FORM_VALUE:
      return {
        ...state,
        loading: false,
        formData: {
          ...state.formData,
          [action.value.inputIdentifier]: {
            ...state[action.value.inputIdentifier],
            value: action.value.value,
            elementType: action.value.elementType,
            validation: action.value.validation,
            touched: action.value.touched,
            elementConfig: action.value.elementConfig
          }
        }
      }
    case actionTypes.SIGN_IN_START:
      return {
        ...state,
        loading: true
      }
    case actionTypes.SIGN_IN_SUCCESS: {
      const expirationDateFormatted = action.expiresIn !== '3600' ? action.expiresIn : new Date(new Date().getTime() + +action.expiresIn)
      return {
        ...state,
        loading: false,
        token: action.token,
        localId: action.localId,
        expirationDate: expirationDateFormatted
      }
    }
    case actionTypes.SIGN_IN_FAIL:
      return {
        ...state,
        loading: false
      }
    case actionTypes.LOG_OUT_SUCCESS:
      return {
        ...state,
        token: null,
        localId: null,
        expirationDate: null
      }
    default:
      return state
  }
}

export default reducer
