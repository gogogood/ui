import * as actionTypes from '../actions/actionTypes'

const initialState = {
  restaurants: [],
  categories: null,
  selectedRestaurant: {},
  selectedCategory: {
    category: null,
    displayValue: null,
    extension: null
  },
  loading: false,
  selectInputData: {
    location: {
      elementType: 'select-perso',
      elementConfig: {
        options: [{ slug: '', displayValue: 'Alors, on bouffe où ?' }]
      },
      validation: {},
      value: ''
    }
  }
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.EMPTY_RESTAURANT_STATE:
      return {
        ...state,
        restaurants: []
      }
    case actionTypes.FETCH_RESTAURANTS_START:
      return {
        ...state,
        loading: true
      }
    case actionTypes.FETCH_RESTAURANTS_SUCCESS:
      return {
        ...state,
        loading: false,
        restaurants: action.restaurants
      }
    case actionTypes.FETCH_RESTAURANTS_FAIL:
      return {
        ...state,
        loading: false
      }
    case actionTypes.FETCH_CATEGORIES_SELECT_INPUT_START:
      return {
        ...state,
        loading: true
      }
    case actionTypes.FETCH_CATEGORIES_SELECT_INPUT_SUCCESS: {
      // let newOptions = [{ slug: '', displayValue: 'Alors, on bouffe où ?'}]
      const newOptions = [{ slug: '', displayValue: 'Qu’est-ce qu\'on s\'enfile today ?' }]
      const newCategories = action.categories.sort((a, b) => {
        if (a.slug < b.slug) { return -1 }
        if (a.slug > b.slug) { return 1 }
        return 0
      })
      newCategories.map(option => {
        newOptions.push(option)
      })
      return {
        ...state,
        loading: false,
        categories: action.categories,
        restaurants: state.restaurants,
        selectInputData: {
          ...state.selectInputData,
          location: {
            ...state.location,
            elementType: 'select-perso',
            elementConfig: {
              ...state.elementConfig,
              options: newOptions
            },
            validation: {},
            value: ''
          }
        }
      }
    }
    case actionTypes.FETCH_CATEGORIES_SELECT_INPUT_FAIL:
      return {
        ...state,
        loading: false
      }
    case actionTypes.UPDATE_SELECTED_CATEGORY:
      return {
        ...state,
        selectedCategory: action.selectedCategory
      }
    case actionTypes.UPDATE_SELECTED_RESTAURANT:
      return {
        ...state,
        selectedRestaurant: action.selectedRestaurant
      }
    case actionTypes.UPDATE_RESTAURANTS: {
      const _ = {
        ...state,
        restaurants: [
          ...state.restaurants
        ]
      }
      _.restaurants.push(action.restaurant)
      return {
        ...state,
        restaurants: _.restaurants
      }
    }
    case actionTypes.UPDATE_RESTAURANT_SUCCESS: {
      const u = {
        ...state,
        restaurants: [
          ...state.restaurants
        ]
      }
      const restaurants = u.restaurants.map(restaurant => {
        if (restaurant._id === action.restaurant._id) {
          restaurant = action.restaurant
          return restaurant
        } else {
          return restaurant
        }
      })
      return {
        ...state,
        restaurants: restaurants
      }
    }
    default:
      return state
  }
}

export default reducer
