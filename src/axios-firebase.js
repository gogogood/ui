import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://gogogood-1565692438265.firebaseio.com/'
})

export default instance
