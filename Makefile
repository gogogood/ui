.PHONY: docker-build
docker-build:
	docker image build -f production.Dockerfile -t registry.gitlab.com/gogogood/ui .

.PHONY: docker-push
docker-push:
	docker image push registry.gitlab.com/gogogood/ui