FROM node:8.16-alpine

RUN mkdir /app

WORKDIR /app

CMD ["npm", "run", "start"]